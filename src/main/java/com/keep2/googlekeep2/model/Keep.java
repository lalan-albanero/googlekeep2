package com.keep2.googlekeep2.model;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Keep {
    String id;
    @NotNull(message = "Title is required field.")
    String title;
    String category;
    String content;
}
