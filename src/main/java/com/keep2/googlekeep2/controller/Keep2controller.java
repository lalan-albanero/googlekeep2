package com.keep2.googlekeep2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.keep2.googlekeep2.service.Keep2service;

@RestController
public class Keep2controller {
    @Autowired
    Keep2service service;

    @GetMapping("/keeps")
    public ResponseEntity<?> getAllKeeps() {
        return ResponseEntity.ok(service.getAllKeeps());
    }

    @GetMapping("/keeps/{id}")
    public ResponseEntity<?> getAllKeeps(@PathVariable("id") String id) {
        return ResponseEntity.ok(service.getKeepByid(id));
    }

}
