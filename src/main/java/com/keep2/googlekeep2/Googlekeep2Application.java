package com.keep2.googlekeep2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Googlekeep2Application {

	public static void main(String[] args) {
		SpringApplication.run(Googlekeep2Application.class, args);
	}

}
