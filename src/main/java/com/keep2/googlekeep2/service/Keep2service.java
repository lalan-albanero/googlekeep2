package com.keep2.googlekeep2.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;

import com.keep2.googlekeep2.model.Keep;

@Service
public class Keep2service {
    Builder webClient = WebClient.builder();

    public List<Keep> getAllKeeps() {
        ResponseEntity<List<Keep>> response = webClient.baseUrl("http://localhost:8080")
                .build()
                .get()
                .uri("/keeps")
                .retrieve().toEntityList(Keep.class).block();
        return response.getBody();
    }

    public Keep getKeepByid(String id) {
        ResponseEntity<Keep> response = webClient.baseUrl("http://localhost:8080")
                .build()
                .get()
                .uri("/keeps/" + id)
                .retrieve().toEntity(Keep.class).block();
        return response.getBody();

    }
}
